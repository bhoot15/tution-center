<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_user = DB::table('users')
        ->where('user_type','=','student')
        ->orWhere('user_type','=','teacher')
        ->count();
        $student = DB::table('students')->count();
        $teacher = DB::table('educationalinfos')->count();
        $total_post = DB::table('jobfeeds')->count();
        $intlist = DB::table('interested_lists')->count();
        
        //return view('home',['total_user' => $total_user,'student' => $student],['teacher' => $teacher],['total_post' => $total_post]);
        
        $cur_total_user = DB::table('users')->whereNull('deleted_at')->count();
        $cur_student = DB::table('students')->whereNull('deleted_at')->count();
        $cur_teacher = DB::table('educationalinfos')->whereNull('deleted_at')->count();
        $cur_total_post = DB::table('jobfeeds')->whereNull('deleted_at')->count();
        $cur_intlist = DB::table('interested_lists')->whereNull('deleted_at')->count();


        return view('home',compact('total_user','student','teacher','total_post','intlist','cur_total_user','cur_student','cur_teacher','cur_total_post','cur_intlist'));
    }
}
