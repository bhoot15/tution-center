<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EducationalInfo extends Model
{
    /*protected $primaryKey = "email";*/
    use SoftDeletes;
    protected $table = "educationalinfos";
    protected $fillable = ['id_card','ssc_year_of_passing','ssc_group','ssc_gpa','hsc_year_of_passing','hsc_group','hsc_gpa','under_grad_year_of_passing','under_grad_major','under_grad_cgpa','grad_year_of_passing','grad_major','grad_cgpa'];
    protected $dates = ['deleted_at'];

}

