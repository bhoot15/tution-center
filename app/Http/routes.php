<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


    Route::auth();

    //Route::get('/register', 'UserController@register');
    Route::resource('/tutor', 'TutorController');
    Route::resource('/', 'HomeController');
    Route::resource('/student', 'StudentController');
    Route::post('/student/search-result', ['as' => 'search', 'uses' => 'StudentController@search']);
    Route::post('/tutor/search-result', ['as' => 'search', 'uses' => 'TutorController@search']);
    Route::post('/post/search-result', ['as' => 'search', 'uses' => 'JobfeedController@search']);
    Route::post('/db/search-result', ['as' => 'search', 'uses' => 'DbController@search']);
    Route::resource('/post', 'JobfeedController');
    Route::resource('/db', 'DbController');
