<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    /*protected  $primaryKey ='email';
    public $incrementing = false;*/
    use SoftDeletes;
    protected $fillable = ['username','active','phone','address','institute'];
    protected $table = "users";
    protected $dates = ['deleted_at'];

    public function setActiveAttribute($value){
        $this->attributes['active'] = ($value);
    }
    public function setUsernameAttribute($value){
        $this->attributes['username'] = ($value);
    }
    public function setPhoneAttribute($value){
        $this->attributes['phone'] = ($value);
    }
    public function setAddressAttribute($value){
        $this->attributes['address'] = ($value);
    }
    public function setInstituteAttribute($value){
        $this->attributes['institute'] = ($value);
    }
}

