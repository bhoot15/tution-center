<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Home extends Model
{
    use SoftDeletes;
    protected $table = "interested_lists";
    protected $fillable = ['username','active'];
    protected $dates = ['deleted_at'];


    public function setActiveAttribute($value){
        $this->attributes['active'] = ($value);
    }
    public function setUsernameAttribute($value){
        $this->attributes['username'] = ($value);
    }
}

