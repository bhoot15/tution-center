@extends('layouts.app')
<style>
    .profile-img{
        width: 200px;
        /*height: 50px;*/
        border: 3px solid;
        box-shadow: 0 2px 2px rgba(0,0,0,0.3);
        display: block;
        margin-left: auto;
        margin-right: auto

    }
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Post</div>

                <div class="panel-body">
                    <center><img class="img-responsive profile-img" src="{{$tutor->profile_pic}}""></center>
                    <br>
                    <form action="/tutor/{{ $tutor->id}}" method="POST">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $tutor->id }}">
                        <div class="form-group">
                            <label for="content">Username</label>
                            <textarea name="username" class="form-control">{{$tutor->username}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <textarea name="phone" class="form-control">{{$tutor->phone}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="address">Email</label>
                            <textarea disabled name="email" class="form-control">{{$tutor->email}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="institute">Institute</label>
                            <textarea name="institute" class="form-control">{{$tutor->institute}}</textarea>
                        </div>
                        <div class="checkbox" name="checkbox">
                            <label>
                                <input type="checkbox" name="active" {{$tutor->active == 1 ? 'checked' : '' }}>
                                Active
                            </label>
                        </div>
                        <input type="submit" value="Update" class="btn btn-success pull-right">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
