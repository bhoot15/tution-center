@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="background-color: #fafafb">
                    <div class="col-lg-12">

                        <form action="/student/search-result" method="post" class="pull-right">
                            {{csrf_field()}}
                            <div class="form-group">

                                <div class="col-md-9">
                                    <input type="text" id="serach-input" name="res" class="form-control" placeholder="Search Student"">
                                </div>

                                <div class="col-md-2">
                                    <button class="btn btn-success" role="button"><i class="fa fa-btn fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel-heading">STUDENTS</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>User Id</th>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Institute</th>
                            <th>User Type</th>
                            <th>Address</th>
                            <th>Gender</th>
                            <th>Ref Name</th>
                            <th>Ref Phone Number</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>View</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($students as $student)
                        <?php if($student->deleted_at == ''){ ?>
                            <tr <?php if($student->active==1){?> class="success"<?php } else { ?> class="danger" <?php } ?>>

                                <td>{{ $student->id }}</td>
                                <td>{{ $student->username }}</td>
                                <td>{{ $student->email }}</td>
                                <td>{{ $student->phone }}</td>
                                <td>{{ $student->institute }}</td>
                                <td>{{ $student->user_type }}</td>
                                <td>{{ $student->detail_address }}</td>
                                <td>{{ $student->gender }}</td>
                                <td>{{ $student->ref_person_name }}</td>
                                <td>{{ $student->ref_phone_no }}</td>
                                <td><?php if($student->active==1){
                                        echo "Activated";
                                    }
                                    else{ echo "Deactivated"; }
                                    ?></td>

                                <td><a href="/student/{{ $student->id }}/edit" class="btn btn-info btn-sm" role="button"><i class="fa fa-btn fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                <td><a href="/student/{{ $student->id }}" class="btn btn-primary btn-sm" role="button"><i class="fa fa-btn fa-search" aria-hidden="true"></i></a></td>
                                <td>
                                    <form action="/student/{{$student->id}}" method="post" class="pull-right">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger btn-sm">
                                            &nbsp;&nbsp;<i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
