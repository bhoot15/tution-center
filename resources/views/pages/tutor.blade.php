@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body" style="background-color: #fafafb">
                    <div class="col-lg-12">

                        <form action="tutor/search-result" method="post" class="pull-right">
                            {{csrf_field()}}
                            <div class="form-group">

                                <div class="col-md-9">
                                    <input type="text" id="serach-input" name="res" class="form-control" placeholder="Search Tutor"">
                                </div>

                                <div class="col-md-2">
                                    <button class="btn btn-success" role="button"><i class="fa fa-btn fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel-heading">TUTORS</div>

                <div class="panel-body">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>User Id</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Gender</th>
                                <th>Address</th>
                                <th>HSC</th>
                                <th>HSC Group</th>
                                <th>HSC GPA</th>
                                <th>Undergrad</th>
                                <th>Major</th>
                                <th>CGPA</th>
                                <th>Institute</th>
                                <th>Status</th>
                                <th>Edit</th>
                                <th>View</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tutors as $tutor)
                            <?php if($tutor->deleted_at == ''){ ?>
                            <tr <?php if($tutor->active==1){?> class="success"<?php } else { ?> class="danger" <?php } ?>>

                                <td>{{ $tutor->id }}</td>
                                <td>{{ $tutor->username }}</td>
                                <td>{{ $tutor->email }}</td>
                                <td>{{ $tutor->phone }}</td>
                                <td>{{ $tutor->gender }}</td>
                                <td>{{ $tutor->detail_address }}</td>
                                <td>{{ $tutor->hsc_year_of_passing }}</td>
                                <td>{{ $tutor->hsc_group }}</td>
                                <td>{{ $tutor->hsc_gpa }}</td>
                                <td>{{ $tutor->under_grad_year_of_passing }}</td>
                                <td>{{ $tutor->under_grad_major }}</td>
                                <td>{{ $tutor->under_grad_cgpa }}</td>
                                <td>{{ $tutor->institute }}</td>

                                <td><?php if($tutor->active==1){
                                        echo "Activated";
                                    }
                                    else{ echo "Deactivated"; }
                                    ?></td>
                                <td><a href="/tutor/{{ $tutor->id }}/edit" class="btn btn-info btn-sm" role="button"><i class="fa fa-btn fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                <td><a href="/tutor/{{ $tutor->id }}" class="btn btn-primary btn-sm" role="button"><i class="fa fa-btn fa-search" aria-hidden="true"></i></a></td>
                                <td>
                                    <form action="tutor/{{$tutor->id}}" method="post" class="pull-right">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger btn-sm">
                                            &nbsp;&nbsp;<i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>

                            </tr>
                            <?php } ?>
                            @endforeach
                            </tbody>
                        </table>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            {{ $tutors->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
