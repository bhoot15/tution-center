<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use DB;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function register()
    {
        return view('auth.register');
    }
}
