@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Post</div>

                <div class="panel-body">
                    <form action="/post/{{ $jobfeed->id}}" method="POST">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}

                        <input type="hidden" name="id" value="{{ $jobfeed->id }}">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <textarea name="username" class="form-control">{{$jobfeed->username}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="title">Title</label>
                            <textarea name="title" class="form-control">{{$jobfeed->title}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="date_to_start">Date to Start</label>
                            <textarea name="date_to_start" class="form-control">{{$jobfeed->date_to_start}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="days_in_week">Days in Week</label>
                            <textarea name="days_in_week" class="form-control">{{$jobfeed->days_in_week}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="preferred_teacher_gender">Preferred Teacher Gender</label>
                            <textarea name="preferred_teacher_gender" class="form-control">{{$jobfeed->preferred_teacher_gender}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="salary">Salary</label>
                            <textarea name="salary" class="form-control">{{$jobfeed->salary}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="num_of_students">No of Students</label>
                            <textarea name="num_of_students" class="form-control">{{$jobfeed->num_of_students}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="preferred_medium">Preferred Medium</label>
                            <textarea name="preferred_medium" class="form-control">{{$jobfeed->preferred_medium}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="student_class">Student Class</label>
                            <textarea name="student_class" class="form-control">{{$jobfeed->student_class}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <textarea name="subject" class="form-control">{{$jobfeed->subject}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea name="address" class="form-control">{{$jobfeed->address}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control">{{$jobfeed->content}}</textarea>
                        </div>
                        <div class="checkbox" name="checkbox">
                            <label>
                                <input type="checkbox" name="active" {{$jobfeed->active == 1 ? 'checked' : '' }}>
                                Active
                            </label>
                        </div>
                        <input type="submit" value="Update" class="btn btn-success pull-right">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
