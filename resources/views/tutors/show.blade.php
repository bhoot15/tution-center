@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7 col-md-offset-2">
            <div class="panel panel-default">
                <form method="get" action="/tutor/{{$user->id}}/edit">
                    <div class="panel-heading">
                        <span>TUTOR INFORMATION</span><br><hr><br>
                    </div>

                    <div class="panel-body">
                        <center><img class="img img-circle" src="{{$user->profile_pic}}" height="100px" width="100px"></center>
                        <br><br>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>Teacher Name</td>
                                <td>{{ $user->username }}</td>
                            </tr>
                            <tr>
                                <td>Teacher Email</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td>Teacher Phone</td>
                                <td>{{ $user->phone }}</td>
                            </tr>
                            <tr>
                                <td>ID Card</td>
                                <td><img class="img img-responsive" src="{{$tutor->id_card}}" height="200px" width="200px"></td>
                            </tr>
                            <!--'hsc_year_of_passing','hsc_group','hsc_gpa','under_grad_year_of_passing','under_grad_major','under_grad_cgpa','grad_year_of_passing','grad_major','grad_cgpa'-->
                            <tr>
                                <td>SSC Passing Year</td>
                                <td>{{ $tutor->ssc_year_of_passing }}</td>
                            </tr>
                            <tr>
                                <td>SSC Group</td>
                                <td>{{ $tutor->ssc_group }}</td>
                            </tr>
                            <tr>
                                <td>SSC GPA</td>
                                <td>{{ $tutor->ssc_gpa }}</td>
                            </tr>
                            <tr>
                                <td>HSC Passing Year</td>
                                <td>{{ $tutor->hsc_year_of_passing }}</td>
                            </tr>
                            <tr>
                                <td>HSC Group</td>
                                <td>{{ $tutor->hsc_group }}</td>
                            </tr>
                            <tr>
                                <td>HSC GPA</td>
                                <td>{{ $tutor->hsc_gpa }}</td>
                            </tr>
                            <tr>
                                <td>Undergraduate Passing Year</td>
                                <td>{{ $tutor->under_grad_year_of_passing }}</td>
                            </tr>
                            <tr>
                                <td>Undergraduate Major</td>
                                <td>{{ $tutor->under_grad_major }}</td>
                            </tr>
                            <tr>
                                <td>Undergraduate CGPA</td>
                                <td>{{ $tutor->under_grad_cgpa }}</td>
                            </tr>
                            <tr>
                                <td>Graduate Passing Year</td>
                                <td>{{ $tutor->grad_year_of_passing }}</td>
                            </tr>
                            <tr>
                                <td>Graduate Major</td>
                                <td>{{ $tutor->grad_major }}</td>
                            </tr>
                            <tr>
                                <td>Graduate CGPA</td>
                                <td>{{ $tutor->grad_cgpa }}</td>
                            </tr>

                            <tr>
                                <td>Detail Address</td>
                                <td>{{ $personalinfo->detail_address }}</td>
                            </tr>
                            <tr>
                                <td>NID/Passport No</td>
                                <td>{{ $personalinfo->nid_passport_no }}</td>
                            </tr>
                            <tr>
                                <td>Facebook ID</td>
                                <td>{{ $personalinfo->fb_id }}</td>
                            </tr>
                            <tr>
                                <td>LinkedIn ID</td>
                                <td>{{ $personalinfo->linkedin_id }}</td>
                            </tr>
                            <tr>
                                <td>Father Name</td>
                                <td>{{ $personalinfo->father_name }}</td>
                            </tr>
                            <tr>
                                <td>Mother Name</td>
                                <td>{{ $personalinfo->mother_name }}</td>
                            </tr>
                            <tr>
                                <td>Father Number</td>
                                <td>{{ $personalinfo->father_number }}</td>
                            </tr>
                            <tr>
                                <td>Mother Number</td>
                                <td>{{ $personalinfo->mother_number }}</td>
                            </tr>
                            <tr>
                                <td>Reference Person Name</td>
                                <td>{{ $personalinfo->ref_person_name }}</td>
                            </tr>
                            <tr>
                                <td>Reference Person Number</td>
                                <td>{{ $personalinfo->ref_person_number }}</td>
                            </tr>
                            <tr>
                                <td>Reference Person Relation</td>
                                <td>{{ $personalinfo->ref_person_relation }}</td>
                            </tr>
                            <tr>
                                <td>Additional Number</td>
                                <td>{{ $personalinfo->additional_number }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer clearfix" style="background-color:white;">
                        <div><input type="submit" class="btn btn-success pull-right" value="Edit"></div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
@endsection

