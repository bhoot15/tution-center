@extends('layouts.app')

@section('content')

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

    <div class="container-fluid" style="background-color: #fafafa ">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Tutor Center</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <br><br>

        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Current Data</h3>
            </div>
            <div class="box-body">
                <section class="content">
                    <!-- Info boxes -->
                    <div class="row">

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{$cur_total_post}}<sup style="font-size: 20px"> posts </sup></h3>

                                    <p>TOTAL POSTS</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-tags" aria-hidden="true"></i>
                                </div>
                                <a href="/post" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-purple-gradient">
                                <div class="inner">
                                    <h3>{{$cur_teacher}}<sup style="font-size: 20px"> teachers </sup></h3>

                                    <p>TOTAL TEACHERS</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <a href="/tutor" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>


                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-teal-gradient">
                                <div class="inner">
                                    <h3>{{$cur_student}}<sup style="font-size: 20px"> students </sup></h3>

                                    <p>TOTAL STUDENTS</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-ios-people-outline"></i>
                                </div>
                                <a href="/student" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>


                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-maroon-gradient">
                                <div class="inner">
                                    <h3>{{$cur_student+$cur_teacher}}<sup style="font-size: 20px"> users </sup></h3>

                                    <p>TOTAL USER</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-lime-active">
                                <div class="inner">
                                    <h3>{{$cur_intlist}}<sup style="font-size: 20px"> peoples </sup></h3>

                                    <p>Interest Shown</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="/db" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>


                </section>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Lifetime Data</h3>
            </div>
            <div class="box-body">
                <section class="content">
                    <!-- Info boxes -->
                    <div class="row">

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-light-blue-gradient">
                                <div class="inner">
                                    <h3>{{$total_post}}<sup style="font-size: 20px"> posts </sup></h3>

                                    <p>TOTAL POSTS</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-tags" aria-hidden="true"></i>
                                </div>
                                <a href="/post" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow-gradient">
                                <div class="inner">
                                    <h3>{{$teacher}}<sup style="font-size: 20px"> teachers </sup></h3>

                                    <p>TOTAL TEACHERS</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <a href="/tutor" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>


                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red-gradient">
                                <div class="inner">
                                    <h3>{{$student}}<sup style="font-size: 20px"> students </sup></h3>

                                    <p>TOTAL STUDENTS</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-ios-people-outline"></i>
                                </div>
                                <a href="/student" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>


                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green-gradient">
                                <div class="inner">
                                    <h3>{{$total_user}}<sup style="font-size: 20px"> users </sup></h3>

                                    <p>TOTAL USER</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-purple-gradient">
                                <div class="inner">
                                    <h3>{{$intlist}}<sup style="font-size: 20px"> peoples </sup></h3>

                                    <p>Interest Shown</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="/db" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> more info</a>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>


                </section>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

<div class="container">
    <footer class="footer">
        <strong>Copyright &copy; <?php echo date('Y')?> <a href="http://troubleshoot-tech.com">Troubleshoot Technologies</a>.</strong> All rights
        reserved.
    </footer>
</div>



<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

@endsection
