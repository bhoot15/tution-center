@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" style="background-color: #fafafb">
                    <div class="col-lg-12">

                        <form action="/db/search-result" method="post" class="pull-right">
                            {{csrf_field()}}
                            <div class="form-group">

                                <div class="col-md-9">
                                    <input type="text" id="serach-input" name="res" class="form-control" placeholder="Search Interested List"">
                                </div>

                                <div class="col-md-2">
                                    <button class="btn btn-success" role="button"><i class="fa fa-btn fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Teacher Email</th>
                            <th>Teacher Name</th>
                            <th>Teacher Phone</th>
                            <th>Post Id</th>
                            <th>Post Title</th>
                            <th>Post Time</th>
                            <th>Student Name</th>
                            <th>Student Phone</th>
                            <th>Salary</th>
                            <th>Date to Start</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($intlists as $intlist)
                        <?php if($intlist->deleted_at == ''){ ?>

                            <tr class="info">
                                <input type="hidden" name="usid" value="{{ $intlist->id }}">
                                <td>{{ $intlist->id }}</td>
                                <td>{{ $intlist->email }}</td>
                                <td>{{ $intlist->username }}</td>
                                <td><a href="tel:+88{{ $intlist->teacher_phone }}" >+88{{ $intlist->teacher_phone }}</td>
                                <td>{{ $intlist->post_id }}</td>
                                <td>{{ $intlist->title }}</td>
                                <td>{{ $intlist->date_time}}</td>
                                <td>{{ $intlist->studentname }}</td>
                                <td><a href="tel:+88{{ $intlist->student_phone }}" >+88{{ $intlist->student_phone }}</a></td>
                                <td>{{ $intlist->salary}}</td>
                                <td>{{ Carbon\Carbon::parse($intlist->date_to_start)->format('l jS F Y') }}</td>
                                <td>
                                    <form action="/db/{{$intlist->id}}" method="post" class="pull-right">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button class="btn btn-danger btn-sm">
                                            &nbsp;&nbsp;<i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            {{ $intlists->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
