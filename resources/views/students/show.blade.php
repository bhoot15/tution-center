@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7 col-md-offset-2">
            <div class="panel panel-default">
                <form method="get" action="/student/{{ $student->id}}/edit">
                    <div class="panel-heading">
                        <span>STUDENT</span><br><hr><br>
                    </div>

                    <div class="panel-body">
                        <center><img class="img img-circle" src="{{$user->profile_pic}}" height="100px" width="100px"></center>
                        <br><br>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>Student Name</td>
                                <td>{{ $user->username }}</td>
                            </tr>
                            <tr>
                                <td>Detail Address</td>
                                <td>{{ $student->detail_address }}</td>
                            </tr>
                            <tr>
                                <td>Student Email</td>
                                <td>{{ $student->email }}</td>
                            </tr>
                            <tr>
                                <td>ID Card</td>
                                <td><img class="img img-responsive" src="{{$student->id_card}}" height="200px" width="200px"></td>
                            </tr>
                            <tr>
                                <td>Reference Person Name</td>
                                <td>{{ $student->ref_person_name }}</td>
                            </tr>
                            <tr>
                                <td>Reference Phone Number</td>
                                <td>{{ $student->ref_phone_no }}</td>
                            </tr>
                            <tr>
                                <td>Reference Person Relation</td>
                                <td>{{ $student->ref_person_relation }}</td>
                            </tr>
                            <tr>
                                <td>Institute Name</td>
                                <td>{{ $student->institute_name }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer clearfix" style="background-color:white;">
                        <div><input type="submit" class="btn btn-success pull-right" value="Edit"></div>
                        <!--                    <div class="pull-left"><input type="submit" class="btn btn-danger pull-right" value="Delete"></div>-->
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
@endsection

