<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Students extends Model
{
    use SoftDeletes;
    protected $fillable = ['id_card','ref_person_name','ref_phone_no','ref_person_relation','institute_name'];
    protected $table = "students";
    protected $dates = ['deleted_at'];
    /*public $incrementing = false;
    protected  $primaryKey ='email';*/

    public function setRefPersonNameAttribute($value){
        $this->attributes['ref_person_name'] = ($value);
    }
    public function setRefPhoneNoAttribute($value){
        $this->attributes['ref_phone_no'] = ($value);
    }
    public function setRefPersonRelationAttribute($value)
    {
        $this->attributes['ref_person_relation'] = ($value);
    }
    public function setInstituteAttribute($value){
        $this->attributes['institute'] = ($value);
    }
}

