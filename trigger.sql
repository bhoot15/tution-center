DELIMITER $$

CREATE
    /*[DEFINER = { user | CURRENT_USER }]*/
    TRIGGER `tutorcentbd_troubles_ged_v1`.`afterUserInsert` AFTER INSERT
    ON `tutorcentbd_troubles_ged_v1`.`users`
    FOR EACH ROW BEGIN
     
     IF new.user_type='student'  THEN 
      INSERT INTO tutorcentbd_troubles_ged_v1.students(id,email)VALUES(new.id, new.email);
     END IF;
     IF new.user_type='teacher'  THEN 
      INSERT INTO tutorcentbd_troubles_ged_v1.educationalinfos(id,email)VALUES(new.id, new.email);
      INSERT INTO tutorcentbd_troubles_ged_v1.personalinfos(id,email)VALUES(new.id, new.email);
      INSERT INTO tutorcentbd_troubles_ged_v1.tutoringinfos(id,email)VALUES(new.id, new.email);
     END IF;

    END$$

DELIMITER ;