<?php

namespace App\Http\Controllers;

use App\Tutor;
use App\EducationalInfo;
use App\PersonalInfo;
use App\TutoringInfo;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;

class TutorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tutors = DB::table('educationalinfos')
            ->join('users', 'users.email', '=', 'educationalinfos.email')
            ->join('personalinfos', 'personalinfos.email', '=', 'users.email')
            ->where('users.user_type', '=', 'teacher')
            ->whereNull('users.deleted_at')
            ->orderBy('users.id','desc')
            ->paginate(5);

        return view('pages.tutor', ['tutors' => $tutors]);
    }

    public function search(Request $request)
    {

        $res = $request->input('res');

        $tutors = DB::table('educationalinfos')
            ->join('users', 'users.email', '=', 'educationalinfos.email')
            ->join('personalinfos', 'personalinfos.email', '=', 'users.email')
            ->where('users.user_type', '=', 'teacher')
            ->where('users.username', 'like', '%'.$res.'%')
            ->orWhere('users.email', 'like','%'.$res.'%')
            ->orWhere('users.id', 'like', $res.'%')
            ->orWhere('users.phone', 'like', '%'.$res.'%')
            ->orWhere('users.institute', 'like', '%'.$res.'%')
            ->whereNull('users.deleted_at')
            ->orderBy('users.id')
            ->get();

        return view('tutors.search-results', compact('tutors','res'));
    }

    public function store(Request $request)
    {
        Tutor::index($request->all());  //method 1 to return full table
    }

    public function show($id)
    {
        $tutor = EducationalInfo::findOrFail($id);
        $personalinfo = PersonalInfo::findOrFail($id);
        $user = User::findOrFail($id);
        return view('tutors.show', compact('tutor','personalinfo','user'));
    }

    public function edit($id)
    {
        $tutor = Tutor::findOrFail($id);
        return view('tutors.edit', compact('tutor'));
    }
    public function update(Request $request, $id)
    {
        $tutor = Tutor::findOrFail($id);
        if(!isset($request->active))
            $tutor->update(array_merge($request->all(),['active'=>0]));
        if(isset($request->active))
            $tutor->update(array_merge($request->all(),['active'=>1]));
        return redirect('/tutor');

    }

    public function destroy($id){
        Tutor::destroy($id);
        EducationalInfo::destroy($id);
        PersonalInfo::destroy($id);
        Tutoringinfo::destroy($id);
        return redirect('/tutor');
    }
}