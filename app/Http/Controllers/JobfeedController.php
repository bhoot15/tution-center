<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Jobfeed;
use DB;
use Auth;

class JobfeedController extends Controller
{
    /**
     * Show a list of all of the application's jobfeeds.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jobfeed = DB::table('jobfeeds')
            ->whereNull('deleted_at')
            ->orderBy('id','desc')
            ->paginate(5);

        //$jobfeed = Jobfeed::paginate(10);

        return view('pages.post', ['jobfeeds' => $jobfeed]);
    }

    public function search(Request $request)
    {

        $res = $request->input('res');
        $jobfeeds = DB::table('jobfeeds')
            ->where('username', 'like', '%'.$res.'%')
            ->orWhere('user_id', 'like','%'.$res.'%')
            ->orWhere('id', 'like', $res.'%')
            ->orWhere('salary', 'like', '%'.$res.'%')
            ->orWhere('preferred_medium', 'like', '%'.$res.'%')
            ->orWhere('student_class', 'like', '%'.$res.'%')
            ->whereNull('deleted_at')
            ->orderBy('id')
            ->get();

        return view('posts.search-result', compact('jobfeeds','res'));

    }

    public function store(Request $request)
    {
        Jobfeed::index($request->all());  //method 1 to return full table
    }

    public function show($id)
    {
        $jobfeed = Jobfeed::findOrFail($id);
        return view('posts.show', compact('jobfeed'));
    }

    public function edit($id)
    {
        $jobfeed = Jobfeed::findOrFail($id);
        return view('posts.edit', compact('jobfeed'));
    }
    public function update(Request $request, $id)
    {
        $student = Jobfeed::findOrFail($id);
        if(!isset($request->active))
            $student->update(array_merge($request->all(),['active'=>0]));
        if(isset($request->active))
            $student->update(array_merge($request->all(),['active'=>1]));
        return redirect('/post');

    }

    public function destroy($id){
        Jobfeed::destroy($id);
        return redirect('/post');
    }
}