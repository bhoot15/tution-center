@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7 col-md-offset-2">
            <div class="panel panel-default">
                <form method="get" action="/post/{{ $jobfeed->id}}/edit">
                    <div class="panel-heading">
                        <span>Edit User Info</span>
                    </div>

                    <div class="panel-body">
                        Name: <span>{{ $jobfeed->username }}</span><br>
                        Email: {{ $jobfeed->email }} <br>
                        Active: <span>{{ $jobfeed->active }}</span>
                    </div>
                    <div class="panel-footer clearfix" style="background-color:white;">
                        <div><input type="submit" class="btn btn-success pull-right" value="Edit"></div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
@endsection
