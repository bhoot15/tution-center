@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-body" style="background-color: #fafafb">
                    <div class="col-lg-12">

                        <form action="/post/search-result" method="post" class="pull-right">
                            {{csrf_field()}}
                            <div class="form-group">

                                <div class="col-md-9">
                                    <input type="text" id="serach-input" name="res" class="form-control" placeholder="Search Post"">
                                </div>

                                <div class="col-md-2">
                                    <button class="btn btn-success" role="button"><i class="fa fa-btn fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel-heading">POST</div>

                <div class="panel-body">

                    <table class="table">
                        <thead>
                        <tr>
                            <th>User Id</th>
                            <th>User Name</th>
                            <th>Date to Start</th>
                            <th>Medium</th>
                            <th>Days in week</th>
                            <th>Teacher Gender</th>
                            <th>Content</th>
                            <th>Salary</th>
                            <th>Student Class</th>
                            <th>Subject</th>
                            <th>Title</th>
                            <th>Post Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($jobfeeds as $jobfeed)
                        <?php if($jobfeed->deleted_at == ''){ ?>
                        <tr <?php if($jobfeed->active==1){?> class="success"<?php } else { ?> class="danger" <?php } ?>>
                            <input type="hidden" name="usid" value="{{ $jobfeed->id }}">
                            <td>{{ $jobfeed->id }}</td>
                            <td>{{ $jobfeed->username }}</td>
                            <td>{{ $jobfeed->date_to_start }}</td>
                            <td>{{ $jobfeed->preferred_medium }}</td>
                            <td>{{ $jobfeed->days_in_week }}</td>
                            <td>{{ $jobfeed->preferred_teacher_gender }}</td>
                            <td>{{ $jobfeed->content }}</td>
                            <td>{{ $jobfeed->salary }}</td>
                            <td>{{ $jobfeed->student_class }}</td>
                            <td>{{ $jobfeed->subject }}</td>
                            <td>{{ $jobfeed->title }}</td>
                            <td>{{ $jobfeed->date_time }}</td>
                            <td><?php if($jobfeed->active==1){
                                    echo "Activated";
                                }
                                    else{ echo "Deactivated"; }
                                 ?></td>
                            <td><a href="/post/{{ $jobfeed->id }}/edit" class="btn btn-success btn-sm" role="button">EDIT</a></td>
                            <td>
                                <form action="post/{{$jobfeed->id}}" method="post" class="pull-right">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger btn-sm">
                                        &nbsp;&nbsp;<i class="fa fa-btn fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        <?php } ?>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            {{ $jobfeeds->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
