<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class PersonalInfo extends Model
{
    /*protected $primaryKey = "email";*/
    use SoftDeletes;
    protected $table = "personalinfos";
    protected $fillable = ['detail_address','nid_passport_no','fb_id','linkedin_id','father_name','mother_name','father_number','mother_number','ref_person_name','ref_person_number','ref_person_relation','additional_number'];
    protected $dates = ['deleted_at'];


}

